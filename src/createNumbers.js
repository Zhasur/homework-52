import React from 'react'

const Number = (props) => {
    return React.createElement('div', {className: 'number'},
        React.createElement('p', {className: 'inner-num'}, props.number));
};

export default Number;