import React, { Component } from 'react';
import './App.css';
import Button from './buttonBlockComponent'
import Number from './createNumbers'

class App extends Component {
  state = {
    array: []
  };


  randNum = () => {
    let newArray = [];
    while (newArray.length < 5) {
      let number = Math.floor(Math.random() * (36 - 5) + 5);

        if (newArray.indexOf(number) === -1) {
            newArray.push(number)
        }

    }

    newArray.sort((a, b) => a - b);

    let copyState = [...this.state.array];
    copyState = newArray;
    this.setState({
        array: copyState
    })


  };

  render() {
    return (
      <div className="App">
        <Button change={this.randNum}/>
          <div className="numbers-block">
              <Number number={this.state.array[0]}/>
              <Number number={this.state.array[1]}/>
              <Number number={this.state.array[2]}/>
              <Number number={this.state.array[3]}/>
              <Number number={this.state.array[4]}/>

          </div>
      </div>
    );
  }
}

export default App;
