import React, {Component} from 'react'

class Button extends Component {
    render() {
        return (
            <div className="button-block">
                <button className="btn" onClick={this.props.change}>New numbers</button>
            </div>
        );
    }
}

export default Button;